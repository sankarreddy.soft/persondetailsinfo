package com.persondetails.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Person implements Serializable {
	@Id
	private int personid;
	private String personfname;
	private String personlname;
	public int getPersonid() {
		return personid;
	}
	public void setPersonid(int personid) {
		this.personid = personid;
	}
	public String getPersonfname() {
		return personfname;
	}
	public void setPersonfname(String personfname) {
		this.personfname = personfname;
	}
	public String getPersonlname() {
		return personlname;
	}
	public void setPersonlname(String personlname) {
		this.personlname = personlname;
	}
	@Override
	public String toString() {
		return "Person [personid=" + personid + ", personfname=" + personfname + ", personlname=" + personlname + "]";
	}
	

}

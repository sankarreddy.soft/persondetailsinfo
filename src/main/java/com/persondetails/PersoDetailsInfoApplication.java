package com.persondetails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersoDetailsInfoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersoDetailsInfoApplication.class, args);
	}

}

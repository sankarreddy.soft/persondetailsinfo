package com.persondetails.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.persondetails.model.Person;



public interface PersonRepo extends JpaRepository<Person, Serializable> {

}

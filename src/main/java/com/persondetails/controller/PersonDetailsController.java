package com.persondetails.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.persondetails.model.Person;
import com.persondetails.service.PersonService;




@RestController
public class PersonDetailsController {
	
	@Autowired
	PersonService personService;
	
	//Add Person 
	@PostMapping("/createperson")
	public int savePerson(@RequestBody Person person) {
		
		return personService.savePerson(person);
		
	}
	//List Persons
	@GetMapping("/persondata")
	private List<Person> getPersonData(){
				
		return personService.getPersonData();
		
	}
	//Delete Person 
	@DeleteMapping("/deleteperson/{personid}")
	private String deletePerson(@PathVariable("personid") int personid){
		try {
		personService.deletePerson(personid);
		
		}catch (Exception e) {
			// TODO: handle exception
			//throw e;
			return e.getMessage();
		}
		String message="the"+personid+"Deleted Successfully";
		return message;
	}
	
	//Edit Person
	@PutMapping("/UpdatePerson")
	private Person editPerson(@RequestBody Person person) {
		
		personService.editPerson(person);
		return person;
		
	}
	
	//Count Persons
		@GetMapping("/countpersons")
		public long personCount() {
			return personService.personCount();
			
		}
	
	

}

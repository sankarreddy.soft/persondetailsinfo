package com.persondetails.controller;


import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.persondetails.model.Person;


@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonaDetailsContollerTest {

	@Autowired
	PersonDetailsController personaDetailsContoller;
	
	@Test
	void testpersonCount() {
		long count =personaDetailsContoller.personCount();
		Assert.assertNotNull(count);
	}
	@Test
	void testSavePerson() {
		
		Person person =new Person();	
		person.setPersonid(110);
		person.setPersonfname("siva");
		person.setPersonlname("reddy");
		int id =personaDetailsContoller.savePerson(person);
		Assert.assertEquals(id, person.getPersonid());
	}

}

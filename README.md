# PersonDetailsInfo
Url to launch the service:
http://localhost:2021

Endpoint:
http://localhost:2021

Service details:

1.Add Person:
http://localhost:2021/createperson

2.Get PersonData
http://localhost:2021/persondata

3.Person Count
http://localhost:2021/countpersons

4.Delete Persondetails
http://localhost:2021/deleteperson/102

5.Edit Persondata
http://localhost:2021/UpdatePerson

Database:
H2 database;
we can find the db properties from application.properties.
Postman refer link:
https://www.getpostman.com/collections/dbc5094002d2ec01fb7a
